## Misc

 * http://www.thestarterkit.info/

## UI Kit

 * http://www.cssflow.com/ui-kits/mobile

## Benchmark

 * http://darsa.in/fpsmeter/

## Icons

 * http://www.pixellove.com
 * http://fontastic.me

## Animations

 * http://ricostacruz.com/jquery.transit/
 * http://tympanus.net/Development/ModalWindowEffects/ (many cool modal appear effects)
 * http://h5bp.github.io/Effeckt.css/
 * http://tympanus.net/Development/PageTransitions/
 * http://lab.hakim.se/meny/
 * http://lab.hakim.se/avgrund/ (popup effect really cool)
 * http://lab.hakim.se/ladda/ (submit button effect with(out) loading)
 * http://lab.hakim.se/kontext/ (only one layout card effect)
 * http://davidwalsh.name/demo/css-flip.php (Flip effect)
 * http://tympanus.net/codrops/2014/05/12/morphing-buttons-concept/
 * http://tympanus.net/Development/ButtonComponentMorph/index3.html
 * http://tympanus.net/Tutorials/AnimatedButtons/
 * http://tympanus.net/Development/CreativeButtons/
 * http://tympanus.net/Development/CreativeButtons/
 *

## CSS formating

 * How to center text verticaly single/multi lines: http://stackoverflow.com/questions/4095165/center-single-and-multi-line-li-text-vertically

## Fonts

 * https://github.com/linjunpop/dynamic-fonts-demo

## Slides

 * https://speakerdeck.com/addyosmani/automating-front-end-workflow
 * http://markdalgleish.github.io/presentation-build-wars-gulp-vs-grunt/

## Javascript encryption

 * RSA: https://github.com/ziyan/javascript-rsa/blob/master/src/rsa.js
 * RSA(2): https://github.com/digitalbazaar/forge/blob/master/js/rsa.js
 * RSA(3): http://www.hanewin.net/encrypt/rsa/rsa.js

## Tasks

 * https://github.com/gulpjs/gulp (http://gulpjs.com)

## Documentation generator

 * https://github.com/visionmedia/dox (http://jsdox.org)
 * https://github.com/punkave/dox-foundation
 * http://usejsdoc.org/tags-example.html
 * https://github.com/senchalabs/jsduck/wiki/Guide#cross-references

## Javascript touch moves

 * https://github.com/jakiestfu/Snap.js

## CSS properties

 * https://developer.mozilla.org/en/docs/Web/CSS/image-rendering

## Mobile special links

 * http://beradrian.wordpress.com/2010/01/15/special-links/