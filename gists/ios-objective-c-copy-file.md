AppDelegate.m
-------------

### Inside `didFinishLaunchingWithOptions` function:

        NSString *docs = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex: 0];
    NSString *dbPathDest = [NSString stringWithFormat:@"%@/%@", docs, @"dicos.db"];

    NSString *dbPathSrc = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"www/data/dicos.db"];

    NSFileManager *fileManager = [NSFileManager defaultManager];

    if ([fileManager fileExistsAtPath:dbPathDest] == NO) {
        NSLog(@"Copying dicos.db file %@", dbPathSrc);
        NSError *error;

        // this fails
        BOOL success = [fileManager copyItemAtPath:dbPathSrc toPath:dbPathDest error:&error];

        if (!success) {
            NSLog(@"failed: %@", [error localizedDescription]);
        }
    }
