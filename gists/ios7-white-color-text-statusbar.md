Project > Info
--------------


Add keys:

 . `View controller-based status bar appearance` to `YES`
 . Inside file `MainViewController.m` add below `@implementation MainViewController`

```objc
    - (UIStatusBarStyle)preferredStatusBarStyle
    {
        return UIStatusBarStyleLightContent;
    }
```
