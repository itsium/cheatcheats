### Disable crash report sender daemon

* Edit `/etc/default/whoopsie`
* Set `report_crashes=false`
* Launch `sudo service whoopsie stop`

****

### Apt-get china mirror

* Edit `/etc/apt/sources.list`
* Add those lines at the beginning of the file (and comment all other lines)

```
deb mirror://mirrors.ubuntu.com/mirrors.txt saucy main restricted universe multiverse
deb mirror://mirrors.ubuntu.com/mirrors.txt saucy-updates main restricted universe multiverse
deb mirror://mirrors.ubuntu.com/mirrors.txt saucy-backports main restricted universe multiverse
deb mirror://mirrors.ubuntu.com/mirrors.txt saucy-security main restricted universe multiverse
```

****

### Configure timeout for apt

* Go to [http://askubuntu.com/questions/141513/how-to-lower-wait-time-for-repository-updates](http://askubuntu.com/questions/141513/how-to-lower-wait-time-for-repository-updates)

or

```shell
sudo emacs /etc/apt/apt.conf.d/99timeout
Acquire::http::Timeout "10";
Acquire::ftp::Timeout "10";
sudo apt-get update
```

****

### Install ruby

* Go to [https://rvm.io/rvm/install](https://rvm.io/rvm/install)

or

```shell
curl -sSL https://get.rvm.io | bash
rvm list
```

****

### Rubygem china mirror
```shell
gem sources --remove https://rubygems.org/
gem sources -a http://ruby.taobao.org/
gem sources -l
gem install rails
```
* You also can edit `Gemfile` and add:
```
source 'http://ruby.taobao.org/'
```

****

### Install nodejs
```shell
curl https://raw.github.com/creationix/nvm/master/install.sh | sh
nvm install v0.10.24
```